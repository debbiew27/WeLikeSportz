Group Members (GitLab ID, EID):

- Debbie Wong: @debbiew27, dw29779
- Victor Xia: @VictorMXia, vmx55
- Bruce Moe: @Bruce-Moe, bam5379
- Ian Winson: @ianwinson, ikw96
- Andrew Wu: @eafdeafd, amw5574

Git SHA Phase 2: 07efbaadc6033481297e603d2a4a12477732906f

Git SHA Phase 3: ca4f3bf8de801a0793186d15c89817ed0b9826e9

GIT SHA Phase 4: 9bd538d3496ee8a10d23dfe4010738569bc6833b

Project Leader Phase 1: Andrew Wu

Project Leader Phase 2: Bruce Moe

Project Leader Phase 3: Debbie Wong

Project Leader Phase 4: Victor Xia

GitLab Pipelines: https://gitlab.com/debbiew27/WeLikeSportz/-/pipelines

Website: welikesportz.me

---

Phase 2:

---

Estimated completion time for each member:

- Debbie - 30 hrs
- Victor - 30 hrs
- Bruce - 30 hrs
- Ian - 30 hrs
- Andrew - 30 hrs

Actual completion time for each member:

- Debbie - 35 hrs
- Victor - 35 hrs
- Bruce - 36 hrs
- Ian - 33 hrs
- Andrew - 34 hrs

---

Phase 3:

---

Estimated completion time for each member:

- Debbie - 13 hrs
- Victor - 12 hrs
- Bruce - 15 hrs
- Ian - 12 hrs
- Andrew - 13 hrs

Actual completion time for each member:

- Debbie - 18 hrs
- Victor - 18 hrs
- Bruce - 20 hrs
- Ian - 17 hrs
- Andrew - 18 hrs

---

Phase 4:

---

Estimated completion time for each member:

- Debbie - 8 hrs
- Victor - 7 hrs
- Bruce - 8 hrs
- Ian - 7 hrs
- Andrew - 7 hrs

Actual completion time for each member:

- Debbie - 7 hrs
- Victor - 6 hrs
- Bruce - 7 hrs
- Ian - 6 hrs
- Andrew - 6 hrs

---

Comments: Our project draws inspiration from Texas Votes(https://gitlab.com/forbesye/fitsbits):

- Our code used to dynamically pull data from Gitlab in About.js was inspired from the About page in TexasVotes
- We used Ant Design as our primary frontend framework
- Our frontend docker image and selenium GUI acceptance tests also referenced TexasVotes in order to set up and pass the Gitlab pipeline
- We used Jest snapshot tests similar to TexasVotes' tests
- We used the use-query-params and react-highlighter packages to implement searching, sorting, and filtering for Phase 3
- For backend, we used the helpful guides from the TA's for our docker images, setting up AWS RDS and PostgreSQL, and our nginx and uwsgi.
- For our visualizations we used Recharts and BubbleChart from react-bubble-chart-d3
