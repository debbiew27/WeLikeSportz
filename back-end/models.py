from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from dotenv import load_dotenv
import os
from pathlib import Path
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump
import csv

load_dotenv(dotenv_path=Path("./dontlook.env"))
app = Flask(__name__)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# Schema: "postgres+psycopg2://<USERNAME>:<PASSWORD>@<IP_ADDRESS>:<PORT>/<DATABASE_NAME>"
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
CORS(app)
db = SQLAlchemy(app)
ma = Marshmallow(app)

###### SCHEMAS ######
class city(db.Model):
    __tablename__ = "city"
    id = db.Column(db.Integer, primary_key=True)
    cities_name = db.Column(db.String())
    cities_state = db.Column(db.String())
    cities_lat = db.Column(db.Float)
    cities_long = db.Column(db.Float)
    cities_population = db.Column(db.Integer)
    cities_numteams = db.Column(db.Integer)
    cities_numsports = db.Column(db.Integer)
    cities_popularsports = db.Column(db.String())
    cities_picture = db.Column(db.String())
    cities_notableplayers = db.relationship("player", backref="city")
    cities_notableteams = db.relationship("team", backref="city")

    def __init__(
        self,
        cities_name="NaN",
        cities_state="NaN",
        cities_lat=0.0,
        cities_long=0.0,
        cities_population=0,
        cities_numteams=0,
        cities_numsports=0,
        citites_popularsports="Nan",
        cities_picture="NaN",
    ):
        self.cities_name = cities_name
        self.cities_state = cities_state
        self.cities_lat = cities_lat
        self.cities_long = cities_long
        self.cities_population = cities_population
        self.cities_numteams = cities_numteams
        self.cities_numsports = cities_numsports
        self.cities_popularsports = citites_popularsports
        self.cities_picture = cities_picture


class team(db.Model):
    __tablename__ = "team"
    id = db.Column(db.Integer, primary_key=True)
    teams_name = db.Column(db.String())
    teams_state = db.Column(db.String())
    teams_city = db.Column(db.String())
    teams_league = db.Column(db.String())
    teams_stadiumcapacity = db.Column(db.Integer)
    teams_sport = db.Column(db.String())
    teams_stadium = db.Column(db.String())
    teams_yearformed = db.Column(db.Integer)
    teams_badge = db.Column(db.String())
    teams_banner = db.Column(db.String())
    teams_logo = db.Column(db.String())
    teams_facebook = db.Column(db.String())
    teams_cityid = db.Column(db.Integer, db.ForeignKey("city.id"))
    teams_notableplayers = db.relationship("player", backref="team")

    def __init__(
        self,
        teams_name="NaN",
        teams_state="NaN",
        teams_city="NaN",
        teams_league="NaN",
        teams_stadiumcapacity=0,
        teams_sport="NaN",
        teams_stadium="NaN",
        teams_yearformed=0,
        teams_badge="NaN",
        teams_banner="NaN",
        teams_logo="NaN",
        teams_facebook="NaN",
    ):
        self.teams_name = teams_name
        self.teams_state = teams_state
        self.teams_city = teams_city
        self.teams_league = teams_league
        self.teams_sport = teams_sport
        self.teams_stadium = teams_stadium
        self.teams_yearformed = teams_yearformed
        self.teams_stadiumcapacity = teams_stadiumcapacity
        self.teams_badge = teams_badge
        self.teams_banner = teams_banner
        self.teams_logo = teams_logo
        self.teams_facebook = teams_facebook


class player(db.Model):
    __tablename__ = "player"
    id = db.Column(db.Integer, primary_key=True)
    players_name = db.Column(db.String())
    players_state = db.Column(db.String())
    players_city = db.Column(db.String())
    players_sport = db.Column(db.String())
    players_birthday = db.Column(db.String())
    players_height = db.Column(db.String())
    players_weight = db.Column(db.String())
    players_team = db.Column(db.String())
    players_origin = db.Column(db.String())
    players_jerseynum = db.Column(db.String())
    players_description = db.Column(db.String())
    players_picture = db.Column(db.String())
    players_facebook = db.Column(db.String())
    players_youtubeid = db.Column(db.String())
    players_teamid = db.Column(db.Integer, db.ForeignKey("team.id"))
    players_cityid = db.Column(db.Integer, db.ForeignKey("city.id"))

    def __init__(
        self,
        players_name="NaN",
        players_state="NaN",
        players_city="NaN",
        players_sport="NaN",
        players_birthday="NaN",
        players_height="NaN",
        players_weight="NaN",
        players_team="NaN",
        players_origin="NaN",
        players_jersey_num="NaN",
        players_description="NaN",
        players_picture="NaN",
        players_facebook="NaN",
        players_youtubeid="NaN",
    ):
        self.players_name = players_name
        self.players_state = players_state
        self.players_city = players_city
        self.players_sport = players_sport
        self.players_birthday = players_birthday
        self.players_height = players_height
        self.players_weight = players_weight
        self.players_team = players_team
        self.players_origin = players_origin
        self.players_jerseynum = players_jersey_num
        self.players_description = players_description
        self.players_picture = players_picture
        self.players_facebook = players_facebook
        self.players_youtubeid = players_youtubeid


class PlayerSchema(ma.Schema):
    id = fields.Int(required=True)
    players_name = fields.Str(required=False)
    players_state = fields.Str(required=False)
    players_city = fields.Str(required=False)
    players_sport = fields.Str(required=False)
    players_birthday = fields.Str(required=False)
    players_height = fields.Str(required=False)
    players_weight = fields.Str(required=False)
    players_team = fields.Str(required=False)
    players_teamid = fields.Int(required=False)
    players_origin = fields.Str(required=False)
    players_jerseynum = fields.Str(required=False)
    players_description = fields.Str(required=False)
    players_picture = fields.Str(required=False)
    players_facebook = fields.Str(required=False)
    players_cityid = fields.Int(required=False)
    players_youtubeid = fields.Str(required=False)


class TeamSchema(ma.Schema):
    id = fields.Int(required=True)
    teams_name = fields.Str(required=False)
    teams_state = fields.Str(required=False)
    teams_city = fields.Str(required=False)
    teams_league = fields.Str(required=False)
    teams_stadiumcapacity = fields.Int(required=False)
    teams_sport = fields.Str(required=False)
    teams_stadium = fields.Str(required=False)
    teams_yearformed = fields.Int(required=False)
    teams_badge = fields.Str(required=False)
    teams_banner = fields.Str(required=False)
    teams_logo = fields.Str(required=False)
    teams_facebook = fields.Str(required=False)
    teams_cityid = fields.Int(required=False)
    teams_notableplayers = fields.Nested(PlayerSchema, many=True)


class CitySchema(ma.Schema):
    id = fields.Int(required=True)
    cities_name = fields.Str(required=False)
    cities_state = fields.Str(required=False)
    cities_lat = fields.Float(required=False)
    cities_long = fields.Float(required=False)
    cities_population = fields.Int(required=False)
    cities_numteams = fields.Int(required=False)
    cities_numsports = fields.Int(required=False)
    cities_popularsports = fields.Str(required=False)
    cities_picture = fields.Str(required=False)
    cities_youtubeid = fields.Str(required=False)
    cities_notableplayers = fields.Nested(PlayerSchema, many=True)
    cities_notableteams = fields.Nested(TeamSchema, many=True)


city_schema = CitySchema()
player_schema = PlayerSchema()
team_schema = TeamSchema()


### UPDATING DB ####


def get_file(file_name):
    with open(file_name, newline="") as f:
        reader = csv.reader(f, delimiter="|")
        data = list(reader)
        return data


def read_csv():
    print("Opening cities csv")
    file_name = "we_like_sportz_cities_bar.csv"
    data = get_file(file_name)
    print("Returned data list:")
    cityList = []
    teamList = []
    # print(data)
    for i in data:
        print("Current Data:")
        print(i)
        print("Current city: ")
        print(i[1])
        cityList.append(i[1])
        new_city = city(i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[10], i[11])
        db.session.add(new_city)

    file_name = "we_like_sportz_teams_bar.csv"
    team_data = get_file(file_name)
    for i in team_data:
        teamList.append(i[1])
        new_team = team(
            i[1], i[2], i[3], i[4], i[5], i[6], i[8], i[9], i[10], i[11], i[12], i[13]
        )
        db.session.add(new_team)

    file_name = "we_like_sportz_players_bar.csv"
    player_data = get_file(file_name)
    for i in player_data:
        print(i)
        new_player = player(
            i[1],
            i[2],
            i[3],
            i[4],
            i[5],
            i[6],
            i[7],
            i[8],
            i[10],
            i[11],
            i[12],
            i[13],
            i[14],
            i[16],
        )
        print("Finished making new player")
        db.session.add(new_player)

    print("No issues")
    db.session.commit()  # Attempt to commit all the records
    # Loop through all cities, grab all teams and players from this city
    for city_name in cityList:
        new_city = db.session.query(city).filter_by(cities_name=city_name)
        teams_with_this_city = db.session.query(team).filter_by(teams_city=city_name)
        for t in teams_with_this_city:
            # new_team = db.session.query(team).filter_by(teams_name = t)
            # new_team.teams_cityid = new_city.id
            t.teams_cityid = new_city.id
            new_city.cities_notableteams.append(t)

        players_with_this_city = db.session.query(player).filter_by(
            players_city=city_name
        )
        for p in players_with_this_city:
            # new_player = db.session.query(player).filter_by(players_name=p)
            # new_player.players_cityid = new_city.id
            p.players_cityid = new_city.id  # unsure
            new_city.cities_notableplayers.append(p)

        # todo: recommit to db? or will it auto update with the append
    print("Filled out info for each city")

    for team_name in teamList:
        new_team = db.session.query(team).filter_by(teams_name=team_name)
        players_with_this_team = db.session.query(player).filter_by(
            players_team=team_name
        )
        for p in players_with_this_team:
            p.players_teamid = new_team.id
            new_team.teams_notableplayers.append(p)

    print("Finished updating db")
    db.session.commit()


### OLD MODELS ###


class WeLikeSportzCities(db.Model):
    id = db.Column(sqlalchemy.Integer, primary_key=True)
    cities_name = db.Column(db.String())
    cities_state = db.Column(db.String())
    cities_lat = db.Column(db.Float)
    cities_long = db.Column(db.Float)
    cities_population = db.Column(db.Integer)
    cities_numteams = db.Column(db.Integer)
    cities_numsports = db.Column(db.Integer)
    cities_notableplayers = db.Column(db.String())
    cities_notableteams = db.Column(db.String())
    cities_popularsports = db.Column(db.String())
    cities_picture = db.Column(db.String())
    cities_youtubeid = db.Column(db.String())

    def __init__(
        self,
        cities_name="NaN",
        cities_state="NaN",
        cities_lat=0.0,
        cities_long=0.0,
        cities_population=0,
        cities_numteams=0,
        cities_numsports=0,
        cities_notableplayers="NaN",
        cities_notableteams="NaN",
        citites_popularsports="Nan",
        cities_picture="NaN",
        cities_youtubeid="NaN",
    ):
        self.cities_name = cities_name
        self.cities_state = cities_state
        self.cities_lat = cities_lat
        self.cities_long = cities_long
        self.cities_population = cities_population
        self.cities_numteams = cities_numteams
        self.cities_numsports = cities_numsports
        self.cities_notableplayers = cities_notableplayers
        self.cities_notableteams = cities_notableteams
        self.cities_popularsports = citites_popularsports
        self.cities_picture = cities_picture
        self.cities_youtubeid = cities_youtubeid


class WeLikeSportzTeams(db.Model):
    id = db.Column(sqlalchemy.Integer, primary_key=True)
    teams_name = db.Column(db.String())
    teams_state = db.Column(db.String())
    teams_city = db.Column(db.String())
    teams_league = db.Column(db.String())
    teams_stadiumcapacity = db.Column(db.Integer)
    teams_sport = db.Column(db.String())
    teams_notableplayers = db.Column(db.String())
    teams_stadium = db.Column(db.String())
    teams_yearformed = db.Column(db.Integer)
    teams_badge = db.Column(db.String())
    teams_banner = db.Column(db.String())
    teams_logo = db.Column(db.String())
    teams_facebook = db.Column(db.String())
    teams_cityid = db.Column(db.Integer)
    teams_youtubeid = db.Column(db.String())

    def __init__(
        self,
        teams_name="NaN",
        teams_state="NaN",
        teams_city="NaN",
        teams_league="NaN",
        teams_stadiumcapacity=0,
        teams_sport="NaN",
        teams_notableplayers="NaN",
        teams_stadium="NaN",
        teams_yearformed=0,
        teams_badge="NaN",
        teams_banner="NaN",
        teams_logo="NaN",
        teams_facebook="NaN",
        teams_cityid=0,
        teams_youtubeid="NaN",
    ):
        self.teams_name = teams_name
        self.teams_state = teams_state
        self.teams_city = teams_city
        self.teams_league = teams_league
        self.teams_sport = teams_sport
        self.teams_notableplayers = teams_notableplayers
        self.teams_stadium = teams_stadium
        self.teams_yearformed = teams_yearformed
        self.teams_stadiumcapacity = teams_stadiumcapacity
        self.teams_badge = teams_badge
        self.teams_banner = teams_banner
        self.teams_logo = teams_logo
        self.teams_facebook = teams_facebook
        self.teams_cityid = teams_cityid
        self.teams_youtubeid = teams_youtubeid


class WeLikeSportzPlayers(db.Model):
    id = db.Column(sqlalchemy.Integer, primary_key=True)
    players_name = db.Column(db.String())
    players_state = db.Column(db.String())
    players_city = db.Column(db.String())
    players_sport = db.Column(db.String())
    players_birthday = db.Column(db.String())
    players_height = db.Column(db.String())
    players_weight = db.Column(db.String())
    players_team = db.Column(db.String())
    players_teamid = db.Column(db.Integer)
    players_origin = db.Column(db.String())
    players_jerseynum = db.Column(db.String())
    players_description = db.Column(db.String())
    players_picture = db.Column(db.String())
    players_facebook = db.Column(db.String())
    players_cityid = db.Column(db.Integer)
    players_youtubeid = db.Column(db.String())

    def __init__(
        self,
        players_name="NaN",
        players_state="NaN",
        players_city="NaN",
        players_sport="NaN",
        players_birthday="NaN",
        players_height="NaN",
        players_weight="NaN",
        players_team="NaN",
        players_teamid=0,
        players_origin="NaN",
        players_jersey_num="NaN",
        players_description="NaN",
        players_picture="NaN",
        players_facebook="NaN",
        players_cityid=0,
        players_youtubeid="NaN",
    ):
        self.players_name = players_name
        self.players_state = players_state
        self.players_city = players_city
        self.players_sport = players_sport
        self.players_birthday = players_birthday
        self.players_height = players_height
        self.players_weight = players_weight
        self.players_team = players_team
        self.players_origin = players_origin
        self.players_jerseynum = players_jersey_num
        self.players_description = players_description
        self.players_picture = players_picture
        self.players_teamid = players_teamid
        self.players_facebook = players_facebook
        self.players_cityid = players_cityid
        self.players_youtubeid = players_youtubeid


### REBUILD  ####


def rebuild_new_db():
    db.create_all()
    db.session.commit()
    cityList = []
    teamList = []
    old_cities = db.session.query(WeLikeSportzCities).all()
    for c in old_cities:
        print("Processing city: " + c.cities_name)
        new_city = city(
            c.cities_name,
            c.cities_state,
            c.cities_lat,
            c.cities_long,
            c.cities_population,
            c.cities_numteams,
            c.cities_numsports,
            c.cities_popularsports,
            c.cities_picture,
        )
        cityList.append(new_city.cities_name)
        db.session.add(new_city)

    old_teams = db.session.query(WeLikeSportzTeams).all()
    for t in old_teams:
        print("Processing team: " + t.teams_name)
        new_team = team(
            t.teams_name,
            t.teams_state,
            t.teams_city,
            t.teams_league,
            t.teams_stadiumcapacity,
            t.teams_sport,
            t.teams_stadium,
            t.teams_yearformed,
            t.teams_badge,
            t.teams_banner,
            t.teams_logo,
            t.teams_facebook,
        )
        teamList.append(new_team.teams_name)
        db.session.add(new_team)

    old_players = db.session.query(WeLikeSportzPlayers).all()
    for p in old_players:
        print("Processing player: " + p.players_name)
        new_player = player(
            p.players_name,
            p.players_state,
            p.players_city,
            p.players_sport,
            p.players_birthday,
            p.players_height,
            p.players_weight,
            p.players_team,
            p.players_origin,
            p.players_jerseynum,
            p.players_description,
            p.players_picture,
            p.players_facebook,
            p.players_youtubeid,
        )
        db.session.add(new_player)

    print("Finished populating new tables with old data")
    # Loop through all cities, grab all teams and players from this city
    for city_name in cityList:
        new_city = db.session.query(city).filter_by(cities_name=city_name).first()
        teams_with_this_city = db.session.query(team).filter_by(teams_city=city_name)
        for t in teams_with_this_city:
            # new_team = db.session.query(team).filter_by(teams_name = t)
            # new_team.teams_cityid = new_city.id
            t.teams_cityid = new_city.id
            new_city.cities_notableteams.append(t)

        players_with_this_city = db.session.query(player).filter_by(
            players_city=city_name
        )
        for p in players_with_this_city:
            # new_player = db.session.query(player).filter_by(players_name=p)
            # new_player.players_cityid = new_city.id
            p.players_cityid = new_city.id  # unsure
            new_city.cities_notableplayers.append(p)

        # todo: recommit to db? or will it auto update with the append
    print("Filled out info for each city")

    for team_name in teamList:
        new_team = db.session.query(team).filter_by(teams_name=team_name).first()
        players_with_this_team = db.session.query(player).filter_by(
            players_team=team_name
        )
        for p in players_with_this_team:
            p.players_teamid = new_team.id
            new_team.teams_notableplayers.append(p)

    print("Finished updating db")
    db.session.commit()
    exit()


"""
class CitySchema(ma.Schema):
    id = fields.Int(required=True)
    cities_name = fields.Str(required=False)
    cities_state = fields.Str(required=False)
    cities_lat = fields.Float(required=False)
    cities_long = fields.Float(required=False)
    cities_population = fields.Int(required=False)
    cities_numteams = fields.Int(required=False)
    cities_numsports = fields.Int(required=False)
    cities_notableplayers = fields.Str(required=False)
    cities_notableteams = fields.Str(required=False)
    cities_popularsports = fields.Str(required=False)
    cities_picture = fields.Str(required=False)
    cities_youtubeid = fields.Str(required=False)

class TeamSchema(ma.Schema):
    id = fields.Int(required=True)
    teams_name = fields.Str(required=False)
    teams_state = fields.Str(required=False)
    teams_city = fields.Str(required=False)
    teams_league = fields.Str(required=False)
    teams_stadiumcapacity = fields.Int(required=False)
    teams_sport = fields.Str(required=False)
    teams_notableplayers = fields.Str(required=False)
    teams_stadium = fields.Str(required=False)
    teams_yearformed = fields.Int(required=False)
    teams_badge = fields.Str(required=False)
    teams_banner = fields.Str(required=False)
    teams_logo = fields.Str(required=False)
    teams_facebook = fields.Str(required=False)
    teams_cityid = fields.Int(required=False)
    teams_youtubeid = fields.Str(required=False)

class PlayerSchema(ma.Schema):
    id = fields.Int(required=True)
    players_name = fields.Str(required=False)
    players_state = fields.Str(required=False)
    players_city = fields.Str(required=False)
    players_sport = fields.Str(required=False)
    players_birthday = fields.Str(required=False)
    players_height = fields.Str(required=False)
    players_weight =fields.Str(required=False)
    players_team = fields.Str(required=False)
    players_teamid = fields.Int(required=False)
    players_origin = fields.Str(required=False)
    players_jerseynum = fields.Str(required=False)
    players_description = fields.Str(required=False)
    players_picture =fields.Str(required=False)
    players_facebook = fields.Str(required=False)
    players_cityid = fields.Int(required=False)
    players_youtubeid = fields.Str(required=False)


#DB Models for WeLikeSportz.

"""
