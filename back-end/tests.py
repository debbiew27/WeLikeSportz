#!/usr/bin/env python3

# --------------
# tests.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

import unittest  # main, TestCase
import json
import urllib
import requests  # for api requests

# -----------
# TestCollatz
# -----------


class UnitTests(unittest.TestCase):
    # ----
    # eval
    # ----

    # def test_max_cycle_length_0(self):
    #     self.assertEqual(Collatz.max_cycle_length(1, 10), 20)

    def test_endpoint1(self):
        url = "http://api.welikesportz.me/cities"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 76)
        self.assertEqual(data["objects"][0]["cities_name"], "Boston")
        self.assertEqual(
            data["objects"][0]["cities_notableplayers"][0]["players_name"],
            "Jaylen Brown",
        )

    def test_endpoint2(self):
        url = "http://api.welikesportz.me/cities/1"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["cities_name"], "Boston")
        self.assertEqual(
            data["cities_notableplayers"][0]["players_name"], "Jaylen Brown"
        )

    def test_endpoint3(self):
        url = "http://api.welikesportz.me/teams"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 111)
        self.assertEqual(data["objects"][0]["teams_city"], "Boston")
        self.assertEqual(data["objects"][0]["teams_name"], "Boston Celtics")

    def test_endpoint4(self):
        url = "http://api.welikesportz.me/teams/1"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["teams_city"], "Boston")
        self.assertEqual(data["teams_name"], "Boston Celtics")

    def test_endpoint5(self):
        url = "http://api.welikesportz.me/players"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 1811)
        self.assertEqual(data["objects"][0]["players_name"], "Lonzo Ball")

    def test_endpoint6(self):
        url = "http://api.welikesportz.me/players/1"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["players_state"], "Louisiana")
        self.assertEqual(data["players_name"], "Lonzo Ball")
        self.assertEqual(data["city"]["cities_name"], "New Orleans")

    # FILTERING CITIES
    def test_endpoint7(self):
        url = 'http://api.welikesportz.me/cities?q={"order_by":[{"field":"cities_name","direction":"asc"}],"filters":[{"name":"cities_state","op":"eq","val":"CA"}]}'
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 11)
        self.assertEqual(data["objects"][0]["cities_name"], "Bakersfield")
        self.assertEqual(
            data["objects"][0]["cities_notableplayers"][0]["players_name"],
            "Josh Currie",
        )

    # FILTERING TEAMS
    def test_endpoint8(self):
        url = 'http://api.welikesportz.me/teams?q={"order_by":[{"field":"teams_name","direction":"asc"}],"filters":[{"name":"teams_league","op":"eq","val":"NBA"}]}'
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 30)
        self.assertEqual(data["objects"][0]["city"]["cities_name"], "Atlanta")
        self.assertEqual(data["objects"][0]["teams_name"], "Atlanta Hawks")
        self.assertEqual(data["objects"][2]["teams_city"], "Brooklyn")
        self.assertEqual(data["objects"][2]["teams_name"], "Brooklyn Nets")
        self.assertEqual(data["objects"][2]["city"]["cities_numteams"], 13)

    # FILTERING PLAYERS
    def test_endpoint9(self):
        url = 'http://api.welikesportz.me/players?q={"order_by":[{"field":"players_name","direction":"asc"}],"filters":[{"name":"players_sport","op":"eq","val":"Basketball"}]}'
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 450)
        self.assertEqual(data["objects"][0]["city"]["cities_name"], "Denver")
        self.assertEqual(data["objects"][0]["players_name"], "Aaron Gordon")
        self.assertEqual(data["objects"][1]["players_city"], "Indianapolis")
        self.assertEqual(data["objects"][1]["players_team"], "Indiana Pacers")
        self.assertEqual(data["objects"][1]["city"]["cities_numteams"], 33)

    # SEARCHING PLAYERS
    def test_endpoint10(self):
        url = "http://api.welikesportz.me/search_players?q=James&page=1&results_per_page=5"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 53)
        self.assertEqual(data["objects"][0]["players_name"], "Erik Spoelstra")
        self.assertEqual(data["objects"][1]["players_name"], "Romeo Langford")
        self.assertEqual(data["objects"][2]["players_name"], "Tristan Thompson")

    # SEARCHING TEAMS
    def test_endpoint11(self):
        url = "http://api.welikesportz.me/search_teams?q=Lakers&page=1&results_per_page=10"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 1)
        self.assertEqual(data["objects"][0]["teams_name"], "Los Angeles Lakers")
        self.assertEqual(
            data["objects"][0]["teams_notableplayers"][0]["players_name"],
            "Markieff Morris",
        )
        self.assertEqual(
            data["objects"][0]["teams_notableplayers"][1]["players_name"],
            "Dennis Schröder",
        )

    # SEARCHING CITIES
    def test_endpoint12(self):
        url = "http://api.welikesportz.me/search_cities?q=CA&page=3&results_per_page=2"
        r = urllib.request.urlopen(url)
        data = json.loads(r.read())
        self.assertEqual(data["num_results"], 39)
        self.assertEqual(data["objects"][0]["cities_name"], "Arlington")
        self.assertEqual(data["objects"][0]["cities_lat"], 32.73569)
        self.assertEqual(data["objects"][0]["cities_long"], -97.10807)


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    unittest.main()
