from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
import unittest
import sys

URL = "https://www.welikesportz.me/"


class NavbarTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testSplash(self):
        self.driver.find_element(By.CLASS_NAME, "Navigation_logo__3uHL1").click()
        element = self.driver.find_element(By.TAG_NAME, "p")
        self.assertEqual(
            element.text,
            "From shooting hoops to the Super Bowl, WeLikeSportz has you covered with the most up-to-date information for the sports you love in the cities you love",
        )

    def testSearch(self):
        self.driver.find_element(By.LINK_TEXT, "Search").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Search")

    def testAbout(self):
        self.driver.find_element(By.LINK_TEXT, "About").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "About Us")

    def testCities(self):
        self.driver.find_element(By.LINK_TEXT, "Cities").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Cities")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    def testPlayers(self):
        self.driver.find_element(By.LINK_TEXT, "Players").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Players")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)

    def testTeams(self):
        self.driver.find_element(By.LINK_TEXT, "Teams").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Teams")

        self.driver.back()
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL)


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
