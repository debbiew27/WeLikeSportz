from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
import unittest
import sys

URL = "https://www.welikesportz.me/"


class SplashTests(unittest.TestCase):
    def setUp(cls):
        service = Service(PATH)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        cls.driver = webdriver.Chrome(service=service, options=chrome_options)
        cls.driver.get(URL)

    def tearDown(cls):
        cls.driver.quit()

    def testCities(self):
        self.driver.find_element(By.ID, "Cities").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Cities")
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "cities")

    def testPlayers(self):
        self.driver.find_element(By.ID, "Players").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Players")
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "players")

    def testTeams(self):
        self.driver.find_element(By.ID, "Teams").click()
        element = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(element.text, "Teams")
        currentURL = self.driver.current_url
        self.assertEqual(currentURL, URL + "teams")


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=["first-arg-is-ignored"])
