import React, { useState, useEffect } from "react";
import styles from "./Cities.module.css";
import Row from "react-bootstrap/Row";
import { columns, attributes } from "./CitiesInfo";
import { filterInfo } from "./CitiesFilter";
import { Spin, Select, Input, Pagination } from "antd";
import { getAPI } from "../library/Data";
import CitiesSearch from "./CitiesSearch";
import CitiesGrid from "./CitiesGrid";
import {
  useQueryParams,
  withDefault,
  StringParam,
  NumberParam,
  ArrayParam,
} from "use-query-params";

const { Option } = Select;
const { Search } = Input;

export default function Cities() {
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [total, setTotal] = useState(0);
  const [searchView, setSearchView] = useState(false);

  const [params, setParams] = useQueryParams({
    search: withDefault(StringParam, ""),
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 10),
    name: StringParam,
    state: withDefault(ArrayParam, []),
    proteams: StringParam,
    sports: withDefault(ArrayParam, []),
    population: StringParam,
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/cities?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    const fetchSearchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/search_cities?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            params.search +
            "&" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    if (params.search !== "") {
      fetchSearchData();
      setSearchView(true);
    } else {
      fetchData();
      setSearchView(false);
    }
  }, [params]);

  useEffect(() => {
    const formatData = async (data) => {
      for (let i = 0; i < data.length; i++) {
        const formatPopulation = data[i]["cities_population"].toLocaleString(
          "en",
          {
            useGrouping: true,
          }
        );
        const formatSports = data[i]["cities_popularsports"]
          .replace(/,\s*$/, "")
          .split(",")
          .join(", ");
        data[i]["cities_population"] = formatPopulation;
        data[i]["cities_popularsports"] = formatSports;
      }
    };
    formatData(data);
  }, [data]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1>Cities</h1>
        <div className={styles.search}>
          <Search
            id="search-bar"
            placeholder="Search Cities"
            defaultValue={params.search}
            enterButton
            size="large"
            onSearch={(value) =>
              setParams((params) => ({ ...params, search: value, page: 1 }))
            }
          />
        </div>
        <Row
          xs={1}
          md={5}
          className="justify-content-md-center row g-2"
          style={{ marginBottom: "30px" }}
        >
          {attributes.map((attribute) => (
            <Select
              id={attribute.name.toLowerCase().replace(/ +/g, "")}
              mode={attribute.multiple === true ? "multiple" : ""}
              allowClear
              value={params[attribute.name.toLowerCase().replace(/ +/g, "")]}
              maxTagCount={0}
              showArrow="true"
              placeholder={attribute.desc}
              onDeselect={(option) => {
                var newParams = {
                  ...params,
                };
                const options =
                  newParams[attribute.name.toLowerCase().replace(/ +/g, "")];
                const index = options.indexOf(option);
                options.splice(index, 1);
                setParams(newParams);
              }}
              onChange={(event) => {
                var newParams = {
                  ...params,
                };
                newParams[attribute.name.toLowerCase().replace(/ +/g, "")] =
                  event;
                setParams(newParams);
              }}
            >
              {attribute.fields.map((field) => (
                <Option disabled={attribute.disabled} value={field}>
                  {field}
                </Option>
              ))}
            </Select>
          ))}
        </Row>
        <Row xs={1} md={1} className="d-flex justify-content-center">
          {loaded ? (
            !searchView ? (
              <CitiesGrid data={data} columns={columns}></CitiesGrid>
            ) : (
              <CitiesSearch
                id="search-view"
                data={data}
                searchQuery={params.search}
              ></CitiesSearch>
            )
          ) : (
            <Spin size="large" />
          )}
        </Row>
        <Pagination
          style={{ marginTop: "40px" }}
          total={total}
          onChange={(pageNumber, pageSize) => {
            setParams((params) => ({
              ...params,
              page: pageNumber,
              perPage: pageSize,
            }));
          }}
          showTotal={(total) => `Total ${total} items`}
          current={params.page}
          pageSize={params.perPage}
        />
      </div>
    </div>
  );
}
