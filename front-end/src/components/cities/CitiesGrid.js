import React from "react";
import { useHistory } from "react-router-dom";
import { Table } from "antd";

export default function CitiesSearch({ data, columns }) {
  const history = useHistory();

  return (
    <div>
      <Table
        onRow={(record) => {
          return {
            onClick: () => {
              document.body.style.cursor = "default";
              history.push("cities/" + record.id);
            },
            onMouseEnter: () => {
              document.body.style.cursor = "pointer";
            },
            onMouseLeave: () => {
              document.body.style.cursor = "default";
            },
          };
        }}
        columns={columns}
        pagination={false}
        dataSource={data}
      />
    </div>
  );
}
