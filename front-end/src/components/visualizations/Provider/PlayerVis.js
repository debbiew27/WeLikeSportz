import React, { useEffect, useState } from "react";
import { getAPI } from "../../library/Data";
import { Spin, Typography } from "antd";
import BubbleChart from "@weknow/react-bubble-chart-d3";

const { Title } = Typography;

export default function PlayerVis() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const parseData = (data) => {
      const output = data.map((player) => {
        return { label: player.player_name, value: player.player_goals };
      });
      return output.slice(0, 20);
    };
    const getData = async () => {
      setLoading(true);
      const goalData = await getAPI(
        "https://api.football-stats.me/players?sort=-goals"
      );
      const data = goalData.data.players;
      const parsedData = parseData(data);
      setData(parsedData);
      setLoading(false);
    };
    getData();
  }, []);

  return (
    <>
      <Title
        style={{
          marginTop: "30px",
          textDecoration: "underline",
          marginBottom: "30px",
        }}
        level={3}
      >
        Player Goals
      </Title>
      {loading ? (
        <Spin> </Spin>
      ) : (
        <div>
          <BubbleChart
            graph={{
              zoom: 1,
            }}
            showLegend={false}
            width={500}
            height={500}
            valueFont={{
              family: "Arial",
              size: 12,
              color: "#fff",
              weight: "bold",
            }}
            labelFont={{
              family: "Arial",
              size: 12,
              color: "#fff",
              weight: "bold",
            }}
            data={data}
          />
        </div>
      )}
    </>
  );
}
