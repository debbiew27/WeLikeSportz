import React from "react";
import styles from "./Home.module.css";
import { Card, Carousel } from "antd";
import luka from "../../resources/luka.png";
import la from "../../resources/lakers.png";
import houston from "../../resources/houston.png";
import brandi from "../../resources/brandi.png";
import jordan from "../../resources/jordan.png";
import tiger from "../../resources/tiger.png";
import gretzky from "../../resources/gretzky.png";
import { useHistory } from "react-router";

import "antd/dist/antd.css";

import Row from "react-bootstrap/Row";

export default function Home() {
  const routerHistory = useHistory();
  const { Meta } = Card;
  return (
    <div className={styles.container}>
      <Carousel autoplay>
        <div className={styles.head}>
          <img alt="jordan" src={jordan} className={styles.headerImage}></img>
          <div className={styles.centered}>
            <h1 id="header" style={{ color: "white" }}>
              WE LIKE SPORTZ
            </h1>
            <h5 style={{ color: "white" }}> and we don't care who knows</h5>
          </div>
        </div>
        <div className={styles.head}>
          <img alt="jordan" src={gretzky} className={styles.headerImage}></img>
          <div className={styles.centered}>
            <div className={styles.white}>
              <h1 style={{ color: "white" }}>WE LIKE SPORTZ</h1>
              <h5 style={{ color: "white" }}> and we don't care who knows</h5>
            </div>
          </div>
        </div>
        <div className={styles.head}>
          <img alt="jordan" src={brandi} className={styles.headerImage}></img>
          <div className={styles.centered}>
            <h1 style={{ color: "white" }}>WE LIKE SPORTZ</h1>
            <h5 style={{ color: "white" }}> and we don't care who knows</h5>
          </div>
        </div>
        <div className={styles.head}>
          <img alt="jordan" src={tiger} className={styles.headerImage}></img>
          <div className={styles.centered}>
            <h1 style={{ color: "white" }}>WE LIKE SPORTZ</h1>
            <h5 style={{ color: "white" }}> and we don't care who knows</h5>
          </div>
        </div>
      </Carousel>
      <div className={styles.wrapper}>
        <div className={styles.header}>
          <div className={styles.body}>
            <p className={styles.description}>
              {" "}
              From shooting hoops to the Super Bowl, WeLikeSportz has you
              covered with the most up-to-date information for the sports you
              love in the cities you love
            </p>
          </div>

          <div className={styles.models}>
            <h3 className={styles.modelsText}> Models </h3>
            <Row xs={1} md={3} className="d-flex justify-content-center">
              {[
                {
                  name: "Cities",
                  image: houston,
                  text: "Find Out Sports Information on different cities",
                  route: "/cities",
                },
                {
                  name: "Players",
                  image: luka,
                  text: "Find lots of stats on players all over the world",
                  route: "/players",
                },
                {
                  name: "Teams",
                  image: la,
                  text: "Find information on different sports teams",
                  route: "/teams",
                },
              ].map((city) => (
                <div
                  key={city.name}
                  id={city.name}
                  className={styles.group}
                  onClick={() => routerHistory.push(city.route)}
                  style={{ cursor: "pointer" }}
                >
                  <Card
                    className={styles.card}
                    style={{ width: 320 }}
                    hoverable={true}
                    cover={
                      <img
                        alt="example"
                        src={city.image}
                        className={styles.cardImage}
                      />
                    }
                  >
                    <Meta title={city.name} description={city.text} />
                  </Card>
                </div>
              ))}
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
}
