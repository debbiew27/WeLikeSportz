import React, { useState, useEffect } from "react";
import styles from "./SearchPage.module.css";
import { Spin, Input } from "antd";
import CitiesSearch from "../cities/CitiesSearch";
import PlayersSearch from "../players/PlayersSearch";
import TeamsSearch from "../teams/TeamsSearch";
import Row from "react-bootstrap/Row";
import { useQueryParams, withDefault, StringParam } from "use-query-params";

import { getAPI } from "../library/Data";

const { Search } = Input;

export default function SearchPage() {
  const numResults = 8;
  const [cityData, setCityData] = useState([]);
  const [teamData, setTeamData] = useState([]);
  const [playerData, setPlayerData] = useState([]);
  const [cityTotal, setCityTotal] = useState(0);
  const [teamTotal, setTeamTotal] = useState(0);
  const [playerTotal, setPlayerTotal] = useState(0);
  const [loaded, setLoaded] = useState(true);

  const [params, setParams] = useQueryParams({
    q: withDefault(StringParam, ""),
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoaded(false);
        const fetchCityData = await getAPI(
          "https://api.welikesportz.me/search_cities?results_per_page=" +
            numResults +
            "&q=" +
            params.q
        );
        const fetchPlayerData = await getAPI(
          "https://api.welikesportz.me/search_players?results_per_page=" +
            numResults +
            "&q=" +
            params.q
        );
        const fetchTeamData = await getAPI(
          "https://api.welikesportz.me/search_teams?results_per_page=" +
            numResults +
            "&q=" +
            params.q
        );
        setCityData(fetchCityData.data.objects);
        setCityTotal(fetchCityData.data.num_results);
        setPlayerData(fetchPlayerData.data.objects);
        setPlayerTotal(fetchPlayerData.data.num_results);
        setTeamData(fetchTeamData.data.objects);
        setTeamTotal(fetchTeamData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, [params]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1 className={styles.title}>Search</h1>
        <div style={{ marginBottom: "20px" }}>
          <Search
            placeholder="Search WeLikeSportz"
            enterButton
            defaultValue={params.q}
            size="large"
            onSearch={(value) => {
              setParams((params) => ({ ...params, q: value }));
            }}
          />
        </div>
      </div>
      <div className={styles.section}>
        <h2 style={{ fontStyle: "italic" }}>Cities</h2>
        <Row xs={1} md={1} className="d-flex justify-content-center">
          {loaded ? (
            <CitiesSearch
              data={cityData}
              searchQuery={params.q}
              link={true}
              total={cityTotal - numResults}
              numResults={numResults}
            />
          ) : (
            <Spin
              size="large"
              style={{ marginTop: "20px", marginBottom: "20px" }}
            />
          )}
        </Row>
      </div>
      <div className={styles.section}>
        <h2 style={{ fontStyle: "italic" }}>Players</h2>
        <Row xs={1} md={1} className="justify-content-md-center">
          {loaded ? (
            <PlayersSearch
              data={playerData}
              searchQuery={params.q}
              link={true}
              total={playerTotal - numResults}
              numResults={numResults}
            />
          ) : (
            <Spin
              size="large"
              style={{ marginTop: "20px", marginBottom: "20px" }}
            />
          )}
        </Row>
      </div>

      <div className={styles.section}>
        <h2 style={{ fontStyle: "italic" }}>Teams</h2>
        <Row xs={1} md={1} className="justify-content-md-center">
          {loaded ? (
            <TeamsSearch
              data={teamData}
              searchQuery={params.q}
              link={true}
              total={teamTotal - numResults}
              numResults={numResults}
            />
          ) : (
            <Spin
              size="large"
              style={{ marginTop: "20px", marginBottom: "20px" }}
            />
          )}
        </Row>
      </div>
    </div>
  );
}
