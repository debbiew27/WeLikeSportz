import React from "react";
import { useHistory } from "react-router-dom";
import { Card, Typography } from "antd";
import Highlighter from "react-highlight-words";
import Row from "react-bootstrap/Row";
import styles from "./Players.module.css";

const { Title, Paragraph } = Typography;

export default function PlayersSearch({
  data,
  searchQuery,
  link,
  total,
  numResults,
}) {
  const history = useHistory();

  return (
    <div>
      {total === -numResults ? (
        <Card className={styles.card} hoverable={true}>
          <Title level={3}>No results found </Title>
        </Card>
      ) : (
        <Row xs={1} md={3} className="d-flex justify-content-center g-3">
          {data.map((player) => (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("players/" + player.id)}
              cover={
                !link ? <img alt="N/A" src={player.players_picture} /> : null
              }
            >
              <Title level={3}>
                {
                  <Highlighter
                    searchWords={searchQuery.split(" ")}
                    textToHighlight={player.players_name}
                  />
                }{" "}
              </Title>
              <Paragraph>
                <Highlighter
                  searchWords={searchQuery.split(" ")}
                  textToHighlight={`${player.players_sport} | ${player.players_state} | ${player.players_city} | ${player.players_team}`}
                />
              </Paragraph>
            </Card>
          ))}
          {link && total > 0 ? (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("players?search=" + searchQuery)}
            >
              <Title level={3}>View {total} more results</Title>
            </Card>
          ) : null}
        </Row>
      )}
    </div>
  );
}
