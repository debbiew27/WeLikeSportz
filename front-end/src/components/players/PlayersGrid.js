import React from "react";
import { useHistory } from "react-router-dom";
import styles from "./Players.module.css";
import { List, Card } from "antd";

const { Meta } = Card;

export default function PlayersGrid({ data }) {
  const history = useHistory();
  return (
    <div>
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 2,
          xl: 3,
          xxl: 3,
        }}
        dataSource={data}
        renderItem={(player) => (
          <List.Item onClick={() => history.push("players/" + player.id)}>
            <Card
              className={styles.card}
              hoverable={true}
              cover={
                <img
                  alt="N/A"
                  src={player.players_picture}
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title={player.players_name}
                description={[
                  player.players_sport,
                  player.players_state,
                  player.players_city,
                  player.players_team,
                ].join(" | ")}
              />
            </Card>
          </List.Item>
        )}
      />
    </div>
  );
}
