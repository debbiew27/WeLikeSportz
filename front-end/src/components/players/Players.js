import React, { useState, useEffect } from "react";
import { Select, Pagination, Spin, Input } from "antd";
import styles from "./Players.module.css";
import Row from "react-bootstrap/Row";
import { attributes } from "./PlayerInfo";
import { filterInfo } from "./PlayerFilter";
import { getAPI } from "../library/Data";
import PlayersGrid from "./PlayersGrid";
import PlayersSearch from "./PlayersSearch";
import {
  useQueryParams,
  StringParam,
  NumberParam,
  ArrayParam,
  withDefault,
} from "use-query-params";

const { Search } = Input;
const { Option } = Select;

export default function Players() {
  const [data, setData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [total, setTotal] = useState(0);
  const [searchView, setSearchView] = useState(false);

  const [params, setParams] = useQueryParams({
    search: withDefault(StringParam, ""),
    page: withDefault(NumberParam, 1),
    perPage: withDefault(NumberParam, 9),
    name: StringParam,
    sport: withDefault(ArrayParam, []),
    city: withDefault(ArrayParam, []),
    team: withDefault(ArrayParam, []),
    jerseynumber: withDefault(ArrayParam, []),
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/players?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    const fetchSearchData = async () => {
      try {
        setLoaded(false);
        const fetchData = await getAPI(
          "https://api.welikesportz.me/search_players?results_per_page=" +
            params.perPage +
            "&page=" +
            params.page +
            "&q=" +
            params.search +
            "&" +
            filterInfo(params)
        );
        setData(fetchData.data.objects);
        setTotal(fetchData.data.num_results);
        setLoaded(true);
      } catch (err) {
        console.error(err);
      }
    };
    if (params.search !== "") {
      fetchSearchData();
      setSearchView(true);
    } else {
      fetchData();
      setSearchView(false);
    }
  }, [params]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.header}>
        <h1>Players</h1>
        <div className={styles.search}>
          <Search
            placeholder="Search Players"
            defaultValue={params.search}
            enterButton
            size="large"
            onSearch={(value) =>
              setParams((params) => ({ ...params, page: 1, search: value }))
            }
          />
        </div>
        <Row
          xs={1}
          md={5}
          className="justify-content-md-center row g-2"
          style={{ marginBottom: "30px" }}
        >
          {attributes.map((attribute) => (
            <Select
              mode={attribute.multiple === true ? "multiple" : ""}
              allowClear
              value={params[attribute.name.toLowerCase().replace(/ +/g, "")]}
              maxTagCount={0}
              showArrow="true"
              placeholder={attribute.desc}
              onDeselect={(option) => {
                var newFilter = {
                  ...params,
                };
                const options =
                  newFilter[attribute.name.toLowerCase().replace(/ +/g, "")];
                const index = options.indexOf(option);
                options.splice(index, 1);
                setParams(newFilter);
              }}
              onChange={(event) => {
                const newFilter = {
                  ...params,
                };
                newFilter[attribute.name.toLowerCase().replace(/ +/g, "")] =
                  event;
                setParams(newFilter);
              }}
            >
              {attribute.fields.map((field) => (
                <Option disabled={attribute.disabled} value={field}>
                  {field}
                </Option>
              ))}
            </Select>
          ))}
        </Row>
        <Row
          xs={1}
          md={1}
          className="d-flex justify-content-center"
          style={{ marginBottom: "30px" }}
        >
          {loaded ? (
            !searchView ? (
              <PlayersGrid data={data}></PlayersGrid>
            ) : (
              <PlayersSearch
                data={data}
                searchQuery={params.search}
              ></PlayersSearch>
            )
          ) : (
            <Spin
              size="large"
              style={{ marginTop: "20px", marginBottom: "20px" }}
            />
          )}
        </Row>
        <Pagination
          total={total}
          onChange={(pageNumber, pageSize) => {
            setParams((params) => ({
              ...params,
              page: pageNumber,
              perPage: pageSize,
            }));
          }}
          showTotal={(total) => `Total ${total} items`}
          current={params.page}
          pageSize={params.perPage}
          pageSizeOptions={[9, 18, 27]}
        />
      </div>
    </div>
  );
}
