import React from "react";
import { useHistory } from "react-router-dom";
import { Card, Typography } from "antd";
import Highlighter from "react-highlight-words";
import Row from "react-bootstrap/Row";
import styles from "./Teams.module.css";

const { Title, Paragraph } = Typography;

export default function TeamsSearch({
  data,
  searchQuery,
  link,
  total,
  numResults,
}) {
  const history = useHistory();

  return (
    <div>
      {total === -numResults ? (
        <Card className={styles.card} hoverable={true}>
          <Title level={3}>No results found </Title>
        </Card>
      ) : (
        <Row xs={1} md={3} className="d-flex justify-content-center g-3">
          {data.map((team) => (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("teams/" + team.id)}
              cover={!link ? <img alt="N/A" src={team.teams_badge} /> : null}
            >
              <Title level={3}>
                {
                  <Highlighter
                    searchWords={searchQuery.split(" ")}
                    textToHighlight={team.teams_name}
                  />
                }{" "}
              </Title>
              <Paragraph>
                <Highlighter
                  searchWords={searchQuery.split(" ")}
                  textToHighlight={`${team.teams_sport} | ${team.teams_state} | ${team.teams_city} | ${team.teams_stadium}`}
                />
              </Paragraph>
            </Card>
          ))}
          {link && total > 0 ? (
            <Card
              className={styles.card}
              hoverable={true}
              onClick={() => history.push("teams?search=" + searchQuery)}
            >
              <Title level={3}>View {total} more results</Title>
            </Card>
          ) : null}
        </Row>
      )}
    </div>
  );
}
