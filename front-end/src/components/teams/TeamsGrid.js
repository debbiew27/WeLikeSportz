import React from "react";
import { useHistory } from "react-router-dom";
import styles from "./Teams.module.css";
import { List, Card } from "antd";

const { Meta } = Card;

export default function TeamsGrid({ data }) {
  const history = useHistory();
  return (
    <div>
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 2,
          xl: 3,
          xxl: 3,
        }}
        dataSource={data}
        renderItem={(team) => (
          <List.Item onClick={() => history.push("teams/" + team.id)}>
            <Card
              className={styles.card}
              hoverable={true}
              cover={
                <img
                  alt="N/A"
                  src={team.teams_badge}
                  className={styles.cardImage}
                />
              }
            >
              <Meta
                title={team.teams_name}
                description={[
                  team.teams_sport,
                  team.teams_state,
                  team.teams_city,
                  team.teams_stadium,
                  team.teams_yearformed,
                ].join(" | ")}
              />
            </Card>
          </List.Item>
        )}
      />
    </div>
  );
}
