import React from "react";
import Splash from "../src/components/splash/Home";
import CitiesInstance from "../src/components/cities/CitiesInstance";
import TeamsInstance from "../src/components/teams/TeamsInstance";
import PlayersInstance from "../src/components/players/PlayerInstance";
import Nav from "../src/components/navigation/Navigation";
import { Card, Carousel, Spin, Table, List } from "antd";
import CitiesSearch from "../src/components/cities/CitiesSearch";
import PlayersSearch from "../src/components/players/PlayersSearch";
import TeamsSearch from "../src/components/teams/TeamsSearch";
import CitiesGrid from "../src/components/cities/CitiesGrid";
import PlayersGrid from "../src/components/players/PlayersGrid";
import TeamsGrid from "../src/components/teams/TeamsGrid";
import { shallow, configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import renderer from "react-test-renderer";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import { QueryParamProvider } from "use-query-params";

configure({ adapter: new Adapter() });

describe("Render components", () => {
  it("renders the carousel", () => {
    const tree = shallow(<Carousel></Carousel>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the spinner", () => {
    const tree = shallow(<Spin></Spin>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the table", () => {
    const tree = shallow(<Table></Table>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the list", () => {
    const tree = shallow(<List></List>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the antd card", () => {
    const tree = shallow(<Card></Card>);
    expect(tree).toMatchSnapshot();
  });
});

describe("Render search views", () => {
  it("renders the cities search view", () => {
    const tree = shallow(
      <CitiesSearch
        data={{}}
        searchQuery=""
        link={false}
        total={0}
        numResults={0}
      ></CitiesSearch>
    );
    expect(tree).toMatchSnapshot();
  });
  it("renders the players search view", () => {
    const tree = shallow(
      <PlayersSearch
        data={{}}
        searchQuery=""
        link={false}
        total={0}
        numResults={0}
      ></PlayersSearch>
    );
    expect(tree).toMatchSnapshot();
  });
  it("renders the teams search view", () => {
    const tree = shallow(
      <TeamsSearch
        data={{}}
        searchQuery=""
        link={false}
        total={0}
        numResults={0}
      ></TeamsSearch>
    );
    expect(tree).toMatchSnapshot();
  });
});

describe("Render instance pages", () => {
  it("renders the cities instance page", () => {
    const tree = renderer
      .create(
        <Router>
          <QueryParamProvider ReactRouterRoute={Route}>
            <Switch>
              <Route path="/cities/:id">
                <CitiesInstance />
              </Route>
            </Switch>
          </QueryParamProvider>
        </Router>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
  it("renders the teams instance page", () => {
    const tree = renderer.create(
      <Router>
        <QueryParamProvider ReactRouterRoute={Route}>
          <Switch>
            <Route path="/teams/:id">
              <TeamsInstance />
            </Route>
          </Switch>
        </QueryParamProvider>
      </Router>
    );
    expect(tree).toMatchSnapshot();
  });
  it("renders the players instance page", () => {
    const tree = renderer.create(
      <Router>
        <QueryParamProvider ReactRouterRoute={Route}>
          <Switch>
            <Route path="/cities/:id">
              <PlayersInstance />
            </Route>
          </Switch>
        </QueryParamProvider>
      </Router>
    );
    expect(tree).toMatchSnapshot();
  });
});

describe("Render home and nav pages", () => {
  it("renders the splash page", () => {
    const tree = shallow(<Splash></Splash>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the nav page", () => {
    const tree = shallow(<Nav></Nav>);
    expect(tree).toMatchSnapshot();
  });
});

describe("Render grid views", () => {
  it("renders the cities grid view", () => {
    const tree = shallow(<CitiesGrid data={{}} columns={[]}></CitiesGrid>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the teams grid view", () => {
    const tree = shallow(<TeamsGrid data={{}} columns={[]}></TeamsGrid>);
    expect(tree).toMatchSnapshot();
  });
  it("renders the players grid view", () => {
    const tree = shallow(<PlayersGrid data={{}} columns={[]}></PlayersGrid>);
    expect(tree).toMatchSnapshot();
  });
});
